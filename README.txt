
Introduction
============

The domain_menu module is a simple module that works with the Domain Access 
suite of modules (http://drupal.org/project/domain). It allows domains to 
assign separate Primary, Secondary and Node Authoring menus. It replicates 
the form found at admin/build/menu/settings. It requires the Domain access and
Menu modules. 

Installation
============

Drop the domain_menu folder in your modules directory (usually
sites/all/modules) and then enable from the modules page. A domain_menu table
will be created and you'll then be able to access the menu settings from the 
/admin/build/domain/view page.  

Basic usage
===========

The basic work flow is as follows:

1) Create separate menus for each site under admin/build/menu. (if you prefer, 
you can use the prefix module and separate them, but you may find that less 
manageable) 

e.g. if you have 3 domains set up (Site One, Site Two and Site Three) your 
menus may look like the following:

  Site One Navigation
    - Primary Navigation
    - Home
    - About
    - Secondary Navigation
    - Contact

  Site Two Navigation
    - Primary Navigation
    - Home
    - About
    - Secondary Navigation
    - Contact

  Site Three Navigation
    - Primary Navigation
    - Home
    - About
    - Secondary Navigation
    - Contact

2) Assign the relevant menus on a per domain basis --
admin/build/domain/menu/$id where $id is the domain id. For example, set the 
Primary Navigation sub-menu that is within the Site One Navigation root menu 
to be the "Menu containing primary links".

3) Restrict parent items to a specific menu on a per domain basis. This means 
that when you are adding/editing a page on the Site One domain you only see 
the following menu:

  Site One Navigation
    - Primary Navigation
    - Home
    - About
    - Secondary Navigation
    - Contact

The settings page differs from the corresponding page in the main drupal system
in one important way; the list of menu options available shows every available 
menu item, not just the root menus. This provides greater flexibility by 
allowing you to assign the primary and secondary links designations to menus 
that are not "root menus". Not only does this prevent you from having to create 
separate root menus, but it is also a convenient work around for this bug [1]

[1] http://drupal.org/node/172081

Thanks
======

Many thanks to agentrickard for the great suite of modules. 

This module was developed in conjunction with mrichar1
(http://drupal.org/user/60123).  
